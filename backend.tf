terraform {
  backend "azurerm" {
    storage_account_name = "backend2022"
    container_name       = "tfstate"
    key                  = "prod.terraform.tfstate"
  }
}
