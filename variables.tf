variable "resource_group_location" {
  description = "Location of the resource group."
}

variable "resource_group_name" {
  description = "name of the resource group for the customer"
}